<?php

$value = "World";

$db = new PDO('mysql:host=database;dbname=mydb;charset=utf8mb4', 'myuser', 'secret');

$databaseTest = ($db->query('SELECT * FROM dockerSample'))->fetchAll(PDO::FETCH_OBJ);

?>

<html>
    <body>
        <h1>Hello, <?= $value ?>!</h1>

        <?php foreach($databaseTest as $row): ?>
            <p>Hello, <?= $row->name ?></p>
        <?php endforeach; ?>

        <?php 
        	 $databaseTest = ($db->query('SELECT pseudo, message FROM messages_chat'))->fetchAll(PDO::FETCH_OBJ);
        ?>
        <?php foreach($databaseTest as $row): ?>
            <p><?= $row->pseudo ?></p>
            <p><?= $row->message ?></p>
        <?php endforeach; ?>
    </body>
    </body>
</html>
